task3(1, 0.5, 10)

function task3(b, m, z1)
    z1 = -z1;
    Tmax = 10; % > mg
    k0 = [3 0];%kp kd
    w0 = sqrt((9.8 * m) / (4 * b));
    T0 = 4 * b * w0^2;
    solution = fminsearch(@(k)(J(T0, m, z1, k, Tmax)), k0);
    [~, T, t, z] = J(T0, m, z1, solution, Tmax); 
    figure('Color', 'w')
    plot(t, T, 'b')
    figure('Color', 'w')
    plot(t, z(1,:), 'b')
end

