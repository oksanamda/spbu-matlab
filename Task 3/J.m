function [J, T, t, z] = J(T0, m, z1, k, Tmax)
    z0 = [0;0];
    t = linspace(0,50,1000);
    zz = @(t,z) [z(2); max(-k(2)*z(2) - k(1)*(z(1)-z1),-Tmax+T0)/m];
    z = Runge_Kutta(t,zz,z0);
    T = k(1)*(z(1,:)-z1) + k(2)*z(2,:) + T0;
    C = T < Tmax;
    T = T.*C + Tmax.*(1-C);
    j = (z(1,:)-z1).^2 + z(2).^2 + T.^2;
    J = trapz(t,j);
    z = -z;
end