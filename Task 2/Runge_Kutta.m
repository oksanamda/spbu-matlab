function y=Runge_Kutta(x,yy,y0)
    h = x(2)-x(1);
    y = zeros(length(y0),length(x));
    y(:,1) = y0;
    for n=2:length(x)
        k1 = yy(x(n-1),y(:,n-1));
        k2 = yy(x(n-1)+h/2,y(:,n-1)+h/2*k1);
        k3 = yy(x(n-1)+h/2,y(:,n-1)+h/2*k2);
        k4 = yy(x(n-1)+h,y(:,n-1)+h*k3);
        y(:,n) = y(:,n-1) + h/6*(k1+2*k2+2*k3+k4);
    end
end
