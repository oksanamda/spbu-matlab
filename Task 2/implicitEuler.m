function yi=implicitEuler(x,yy,y0)
    h = x(2)-x(1);
    yi = zeros(length(y0),length(x));
    yi(:,1) = y0;
    for n=2:length(x)
        func = @(y) (y-yi(:,n-1)-h*yy(x(n), y));
        yi(:,n) = fsolve(func, yi(:, n-1) + h*yy(x(n-1), yi(:, n-1)));
    end  
end