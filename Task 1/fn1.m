function dydx = fn1(x,y)
dydx = sqrt(1 - y^2);
end